## Python Token Exchanger

Simple application for performing token exchange operations for a client id and client secret.

### Installation

```bash
# Install in a virtualenv
pip install -r requirements.txt
```


### Usage

```bash
python exchange.py --help                     
```

```
usage: exchange.py [--help] [--realm REALM] [--server SERVER]
                   client_id client_secret client_exchange

positional arguments:
  client_id        Your client ID
  client_secret    Your client secret
  client_exchange  The client ID for which you want to exchange the token

optional arguments:
  -h, --help       show this help message and exit
  --realm REALM    The keycloak realm, default: cern
  --server SERVER  The keycloak server, default: keycloak-dev.cern.ch
```

Example:


```bash
 python exchange.py \
 "identities-management-agent" \
 "869ffafa-86fe-4296-8769-1c377d8dcb5d" \
 "authorization-service-api"
```

Output: 
```
[x] Token endpoint: https://keycloak-dev.cern.ch/auth/realms/cern/protocol/openid-connect/token
[x] Getting token for identities-management-agent and then exchanging it for authorization-service-api
[x] Token obtained, exchanging it...
[x] Exchanged token obtained, printing it...
{'acr': '1',
 'allowed-origins': ['https://authorization-service-api.web.cern.ch',
                     'https://authorization-service-api-qa.web.cern.ch/',
                     'http://cristi-nuc.cern.ch:5000',
                     'https://authorization-service-api-dev.web.cern.ch',
                     'http://localhost:5000',
                     'http://localhost',
                     'http://npm-registry-1.cern.ch:8080',
                     'https://authorization-service-api-qa.web.cern.ch',
                     'https://localhost:44366'],
 'aud': ['authorization-service-api', 'account'],
 'auth_time': 0,
 'azp': 'identities-management-agent',
 'client_id': 'identities-management-agent',
 'email': 'service-account-identities-management-agent@placeholder.org',
 'exp': 1571667857,
 'iat': 1571667797,
 'iss': 'https://keycloak-dev.cern.ch/auth/realms/cern',
 'jti': '4e49b278-cdca-4558-99d8-ddf449fbfd08',
 'nbf': 0,
 'realm_access': {'roles': ['offline_access', 'uma_authorization']},
 'resource_access': {'account': {'roles': ['manage-account',
                                           'manage-account-links',
                                           'view-profile']}},
 'scope': 'oidc-cern-profile oidc-client-id oidc-identity-provider '
          'oidc-cern-login-info oidc-email',
 'session_state': '2910ce4e-aece-4007-bc02-6c3ce9725645',
 'sub': 'c440a0a3-30b6-47e7-8914-f1bb96c35324',
 'typ': 'Bearer'}

 ```
