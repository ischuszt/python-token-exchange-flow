import argparse
from pprint import pprint
from jwt import JWT
import base64
import requests

SERVER = "keycloak-dev.cern.ch"
REALM = "cern"
REALM_PREFIX = "auth/realms/{}"
TOKEN_ENDPOINT = "protocol/openid-connect/token"
SUBJECT_TOKEN_TYPE = "urn:ietf:params:oauth:token-type:access_token"
TOKEN_EXCHANGE_GRANT_TYPE = "urn:ietf:params:oauth:grant-type:token-exchange"


def get_token_endpoint(args):
    """
    Gets the token enpdoint path from the args
    """
    return "https://{}/{}/{}".format(
        args.server, REALM_PREFIX.format(args.realm), TOKEN_ENDPOINT
    )


parser = argparse.ArgumentParser()
parser.add_argument("client_id", help="Your client ID")
parser.add_argument("client_secret", help="Your client secret")
parser.add_argument(
    "client_exchange", help="The client ID for which you want to exchange the token"
)
parser.add_argument(
    "--realm", help="The keycloak realm, default: cern", type=str, default="cern"
)
parser.add_argument(
    "--server",
    type=str,
    help="The keycloak server, default: {}".format(SERVER),
    default=SERVER,
)
args = parser.parse_args()

print(f"[x] Token endpoint: {get_token_endpoint(args)}")


print(
    "[x] Getting token for {} and then exchanging it for {}".format(
        args.client_id, args.client_exchange
    )
)

r = requests.post(
    get_token_endpoint(args),
    auth=(args.client_id, args.client_secret),
    data={"grant_type": "client_credentials"},
)

if not r.ok:
    print("ERROR getting token: {}".format(r.json()))
    exit(1)


token = r.json()["access_token"]
print("[x] Token obtained, exchanging it...")


r = requests.post(
    get_token_endpoint(args),
    auth=(args.client_id, args.client_secret),
    data={
        "client_id": args.client_id,
        "grant_type": TOKEN_EXCHANGE_GRANT_TYPE,
        "audience": args.client_exchange,
        "subject_token": token,
        "subject_token_type": SUBJECT_TOKEN_TYPE,
    },
)

if not r.ok:
    print("ERROR getting token: {}".format(r.json()))
    exit(1)

print("[x] Exchanged token obtained, printing it...")

jwt = JWT()
pprint(jwt.decode(r.json()["access_token"], do_verify=False))

